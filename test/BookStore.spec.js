import { BookStore } from "../src/BookStore";

const anyError = () => Error("any error");

const resolved = (value) => Promise.resolve(value);

const rejected = (error) => Promise.reject(error);

const includeAllminScore = () => 0;

const aLittleBitAbove = (minScore) => minScore + 0.01;

const aLittleBitBelow = (minScore) => minScore - 0.01;

const anyminScore = 0.5;

describe("BookStore", () => {
  let friendsQueries;
  let getCurrentUser;

  beforeEach(() => {
    let mockedUserPromise;
    getCurrentUser = () => mockedUserPromise;
    getCurrentUser.nextVisit = (userPromise) => {
      mockedUserPromise = userPromise;
    };
    friendsQueries = new BookStore({ getCurrentUser });
  });

  describe("potential book interests", () => {
    it("should find no potential interests if current user has no friends", async () => {
      const user = {
        ratings: [],
        friends: [],
      };
      getCurrentUser.nextVisit(resolved(user));

      const possibleNextReader = await friendsQueries.getPossibleNextReader(
        includeAllminScore()
      );

      expect(possibleNextReader).toBeDefined();
      expect(possibleNextReader.length).toEqual(0);
    });

    it("should find no potential interests if user's friends rate no books", async () => {
      const user = {
        ratings: [],
        friends: [
          {
            id: "friend1",
            ratings: [],
          },
        ],
      };
      getCurrentUser.nextVisit(resolved(user));

      const possibleNextReader = await friendsQueries.getPossibleNextReader(
        includeAllminScore()
      );

      expect(possibleNextReader).toBeDefined();
      expect(possibleNextReader.length).toEqual(0);
    });

    it("potential interests should include books rated by friends", async () => {
      const user = {
        ratings: [],
        friends: [
          {
            id: "friend1",
            ratings: [
              {
                title: "Gone with the Wind",
                score: 0.8,
              },
            ],
          },
          {
            id: "friend2",
            ratings: [
              {
                title: "One Hundred Years of Solitude",
                score: 0.6,
              },
            ],
          },
        ],
      };
      getCurrentUser.nextVisit(resolved(user));

      const possibleNextReader = await friendsQueries.getPossibleNextReader(
        includeAllminScore()
      );

      expect(possibleNextReader).toBeDefined();
      expect(possibleNextReader.length).toEqual(2);
      expect(possibleNextReader).toContain("Gone with the Wind");
      expect(possibleNextReader).toContain("One Hundred Years of Solitude");
    });

    it("potential interests should include only books rated by friends above a minimal score", async () => {
      const user = {
        ratings: [],
        friends: [
          {
            id: "friend1",
            ratings: [
              {
                title: "A Tale of Two Cities",
                score: 0.9,
              },
            ],
          },
          {
            id: "friend2",
            ratings: [
              {
                title: "A Tale of Two Cities",
                score: 0.6,
              },
              {
                title: "Harry Potter and the Prisoner of Azkaban",
                score: 0.5,
              },
            ],
          },
          {
            id: "friend3",
            ratings: [
              {
                title: "A Tale of Two Cities",
                score: 0.8,
              },
              {
                title: "Harry Potter and the Prisoner of Azkaban",
                score: 0.6,
              },
              {
                title: "The Lord of the Rings",
                score: 0.8,
              },
            ],
          },
        ],
      };
      getCurrentUser.nextVisit(resolved(user));

      const possibleNextReader = await friendsQueries.getPossibleNextReader(
        aLittleBitAbove(2 / 3)
      );

      expect(possibleNextReader).toBeDefined();
      expect(possibleNextReader.length).toEqual(2);
      expect(possibleNextReader).toContain("A Tale of Two Cities");
      expect(possibleNextReader).not.toContain(
        "Harry Potter and the Prisoner of Azkaban"
      );
      expect(possibleNextReader).toContain("The Lord of the Rings");
    });

    it("potential interests should not include books already rated by user", async () => {
      const user = {
        ratings: [
          {
            title: "Harry Potter and the Prisoner of Azkaban",
            score: 0.8,
          },
        ],
        friends: [
          {
            id: "friend1",
            ratings: [
              {
                title: "A Tale of Two Cities",
                score: 0.8,
              },
            ],
          },
          {
            id: "friend2",
            ratings: [
              {
                title: "Harry Potter and the Prisoner of Azkaban",
                score: 0.6,
              },
            ],
          },
        ],
      };
      getCurrentUser.nextVisit(resolved(user));

      const possibleNextReader = await friendsQueries.getPossibleNextReader(
        includeAllminScore()
      );

      expect(possibleNextReader).toBeDefined();
      expect(possibleNextReader.length).toEqual(1);
      expect(possibleNextReader).toContain("A Tale of Two Cities");
      expect(possibleNextReader).not.toContain(
        "Harry Potter and the Prisoner of Azkaban"
      );
    });

    it("potential interests should be ordered by popularity among friends (score)", async () => {
      const user = {
        ratings: [],
        friends: [
          {
            id: "friend1",
            ratings: [
              {
                title: "A-Like Title",
                score: 0.9,
              },
              {
                title: "B-Like Title",
                score: 0.6,
              },
              {
                title: "C-Like Title",
                score: 0.8,
              },
            ],
          },
          {
            id: "friend2",
            ratings: [
              {
                title: "A-Like Title",
                score: 0.8,
              },
              {
                title: "B-Like Title",
                score: 0.6,
              },
            ],
          },
          {
            id: "friend3",
            ratings: [
              {
                title: "B-Like Title",
                score: 0.8,
              },
            ],
          },
        ],
      };
      getCurrentUser.nextVisit(resolved(user));

      const possibleNextReader = await friendsQueries.getPossibleNextReader(
        includeAllminScore()
      );

      expect(possibleNextReader).toBeDefined();
      expect(possibleNextReader).toEqual([
        "A-Like Title",
        "C-Like Title",
        "B-Like Title",
      ]);
    });

    it("potential interests of same popularity (score) should be ordered by title", async () => {
      const user = {
        ratings: [],
        friends: [
          {
            id: "friend1",
            ratings: [
              {
                title: "The Lord of the Rings",
                score: 0.8,
              },
              {
                title: "Gone with the Wind",
                score: 0.6,
              },
              {
                title: "One Hundred Years of Solitude",
                score: 0.4,
              },
            ],
          },
          {
            id: "friend2",
            ratings: [
              {
                title: "One Hundred Years of Solitude",
                score: 0.8,
              },
              {
                title: "Gone with the Wind",
                score: 0.6,
              },
              {
                title: "The Lord of the Rings",
                score: 0.4,
              },
            ],
          },
        ],
      };
      getCurrentUser.nextVisit(resolved(user));

      const possibleNextReader = await friendsQueries.getPossibleNextReader(
        includeAllminScore()
      );

      expect(possibleNextReader).toBeDefined();
      expect(possibleNextReader).toEqual([
        "One Hundred Years of Solitude",
        "The Lord of the Rings",
        "Gone with the Wind",
      ]);
    });
  });

  describe("fetch failure", () => {
    it("should return no matches if user fetch failed", async () => {
      getCurrentUser.nextVisit(rejected(anyError()));

      const possibleNextReader = await friendsQueries.getPossibleNextReader(
        anyminScore
      );

      expect(possibleNextReader).toEqual([]);
    });

    it("if possible should use previously fetched user if current user fetch failed (changed minimal score)", async () => {
      const user = {
        ratings: [],
        friends: [
          {
            id: "friend1",
            ratings: [
              {
                title: "More Popular Book",
                score: 0.8,
              },
              {
                title: "Less Popular Book",
                score: 1 / 3,
              },
            ],
          },
          {
            id: "friend2",
            ratings: [
              {
                title: "More Popular Book",
                score: 0.8,
              },
            ],
          },
          {
            id: "friend3",
            ratings: [],
          },
        ],
      };
      getCurrentUser.nextVisit(resolved(user));
      await friendsQueries.getPossibleNextReader(anyminScore);

      getCurrentUser.nextVisit(rejected(anyError()));
      const potentialInterests1 = await friendsQueries.getPossibleNextReader(
        aLittleBitAbove(1 / 3)
      );

      expect(potentialInterests1).toEqual(["More Popular Book"]);

      // and when
      getCurrentUser.nextVisit(rejected(anyError()));
      const potentialInterests2 = await friendsQueries.getPossibleNextReader(
        aLittleBitBelow(1 / 3)
      );

      expect(potentialInterests2).toEqual([
        "More Popular Book",
        "Less Popular Book",
      ]);
    });
  });

  describe("invalid data", () => {
    it("should find no potential interests if current user has no 'friends' field", async () => {
      const user = {
        ratings: [],
        // no 'friends' field
      };
      getCurrentUser.nextVisit(resolved(user));

      const possibleNextReader = await friendsQueries.getPossibleNextReader(
        includeAllminScore()
      );

      expect(possibleNextReader).toEqual([]);
    });

    it("should not fail if some friend has no 'ratings' field", async () => {
      const user = {
        ratings: ["Book2"],
        friends: [
          {
            id: "friendWithRatings",
            ratings: [
              {
                title: "Book1",
                score: 0.8,
              },
            ],
          },
          {
            id: "friendWithoutRatings",
            // no 'ratings' field
          },
        ],
      };
      getCurrentUser.nextVisit(resolved(user));

      const possibleNextReader = await friendsQueries.getPossibleNextReader(
        includeAllminScore()
      );

      expect(possibleNextReader).toEqual(["Book1"]);
    });

    it("should filter out duplicated books of same friend", async () => {
      const user = {
        ratings: [],
        friends: [
          {
            id: "friend1",
            ratings: [
              {
                title: "BookScoredAboveMinimal",
                score: 0.8,
              },
              {
                title: "BookScoredAboveMinimal",
                score: 0.6,
              },
              {
                title: "BookScoredAboveMinimal",
                score: 0.4,
              },
            ],
          },
          {
            id: "friend2",
            ratings: [
              {
                title: "BookScoredAboveMinimal",
                score: 0.6,
              },
            ],
          },
        ],
      };
      getCurrentUser.nextVisit(resolved(user));

      const possibleNextReader = await friendsQueries.getPossibleNextReader(
        aLittleBitAbove(1 / 2)
      );

      expect(possibleNextReader).toEqual(["BookScoredAboveMinimal"]);
    });
  });
});
