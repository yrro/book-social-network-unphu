# UNPHU Book Online Store: Find possible readers (interested)

## Introduction

* A book is considered a potential interest if it is rated by at least one of the user's friends, and each user will give it a `score`. Then the recommendations can be limited by a minimum score (`minScore`).

* A book is **not** considered a potential interest if it is already rated by a user.

### Ordering of potential interests

* A book with a higher average score (rated by more friends) should be placed before a book with a lower score.

## Setup

1. `npm install` – install dependencies
2. `npm test` – run all tests
3. `npm run test:watch` – run all tests in _watch mode_ (alternative to `npm test`)
4. `nvm install` - (optional) set up the expected _major_ version of Node.js locally ([`nvm`](https://github.com/nvm-sh/nvm) required; Node.js version defined in `.nvmrc` file)


## Notes
* Find the `BookStore.js` class under the `src` folder
* There's a `BookStore.spec.js` file which contains related unit tests