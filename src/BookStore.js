class BookStore {
    // Constructor that takes a function to get the current user.
    constructor({ getCurrentUser }) {
        this.getCurrentUser = getCurrentUser;
        this.user = null; // Initialize user property to null.
    }

    // Method to get the possible next reader with a minimum score filter.
    async getPossibleNextReader(minScore) {
        try {
            // Attempt to get the current user asynchronously.
            const currentUser = await this.getCurrentUser();
            this.user = currentUser; // Store the current user.
            // Calculate the possible next reader based on the current user and minimum score.
            return this.calculatePossibleNextReader(currentUser, minScore);
        } catch (error) {
            // If there is an error and the user property is set, use the stored user.
            if (this.user) {
                return this.calculatePossibleNextReader(this.user, minScore);
            } else {
                // If there is no user stored, return an empty array.
                return [];
            }
        }
    }

    // Method to calculate the possible next reader based on the user and minimum score.
    calculatePossibleNextReader(user, minScore) {
        // If the user has no friends or an empty friends list, return an empty array.
        if (!user.friends || user.friends.length === 0) {
            return [];
        }

        // Create a set of titles the user has rated.
        const potentialRate = new Set(user.ratings?.map(r => r.title));

        // Map to store the possible next reader ratings.
        const possibleNextReader = new Map();

        // Iterate over the user's friends.
        user.friends.forEach((friend) => {
            if (friend.ratings) {
                // Iterate over the friend's ratings.
                friend.ratings.forEach((rating) => {
                    // If the user has not rated the title.
                    if (!potentialRate.has(rating.title)) {
                        const existingRating = possibleNextReader.get(rating.title);
                        if (existingRating) {
                            // If the title is already in the map, update the score and count.
                            existingRating.totalScore += rating.score;
                            existingRating.friendCount += 1;
                        } else {
                            // If the title is not in the map, add it with the initial score and count.
                            possibleNextReader.set(rating.title, {
                                totalScore: rating.score,
                                friendCount: 1,
                            });
                        }
                    }
                });
            }
        });

        // Organize the next readers by filtering, mapping, and sorting.
        const organizedNextReaders = Array.from(possibleNextReader.entries())
            .filter(([_, data]) => data.friendCount > 0) // Ensure there is at least one rating.
            .map(([title, data]) => ({ title, averageScore: data.totalScore / data.friendCount })) // Calculate average score.
            .filter((interest) => interest.averageScore >= minScore) // Filter by minimum score.
            .sort((a, b) => {
                if (a.averageScore !== b.averageScore) {
                    return b.averageScore - a.averageScore; // Sort by score descending.
                }
                return a.title.localeCompare(b.title, "en", { sensitivity: "base" }); // Sort by title alphabetically.
            })
            .map((interest) => interest.title); // Map to titles.

        return organizedNextReaders; // Return the sorted list of titles.
    }
}

module.exports = { BookStore };
